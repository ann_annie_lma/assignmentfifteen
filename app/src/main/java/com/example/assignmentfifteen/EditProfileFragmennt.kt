package com.example.assignmentfifteen

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.asLiveData
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.example.assignmentfifteen.databinding.FragmentEditProfileFragmenntBinding
import com.example.assignmentfifteen.databinding.FragmentProfileBinding
import kotlinx.coroutines.launch


class EditProfileFragmennt : Fragment() {

    private var _binding: FragmentEditProfileFragmenntBinding?  = null
    val binding get() = _binding!!

    lateinit var userInfoManager: UserInfoManager
    var firstName = ""
    var lastName = ""
    var age = 0
    var gender = ""
    var email = ""
    var phone = ""
    var address = ""

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentEditProfileFragmenntBinding.inflate(inflater,container,false)
        userInfoManager = UserInfoManager(requireContext())
        return _binding!!.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        observerData()

        saveUserInfo()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun saveUserInfo()
    {
        binding.btnUpdate.setOnClickListener {

            firstName = binding.etFirstName.text.toString()
            lastName = binding.etLastName.text.toString()
            age = binding.etAge.text.toString().toInt()
            gender = binding.etGender.text.toString()
            email = binding.etEmail.text.toString()
            phone = binding.etPhoneNumber.text.toString()
            address = binding.etAddress.text.toString()


            lifecycleScope.launch {
                userInfoManager.storeUser(firstName,lastName,age,gender,email,phone,address)
            }



            val action = EditProfileFragmenntDirections.actionEditProfileFragmenntToProfileFragment()
            findNavController().navigate(action)
        }


    }

    private fun observerData()
    {
        userInfoManager.userFirstNameFlow.asLiveData().observe(viewLifecycleOwner,{
            firstName = it
            binding.etFirstName.setText(it)
        })

        userInfoManager.userLastNameFlow.asLiveData().observe(viewLifecycleOwner,{
            lastName = it
            binding.etLastName.setText(it)
        })

        userInfoManager.userGenderFlow.asLiveData().observe(viewLifecycleOwner,{
            gender = it
            binding.etGender.setText(it)
        })

        userInfoManager.userAgeFlow.asLiveData().observe(viewLifecycleOwner,{
            age = it
            binding.etAge.setText(it.toString())
        })

        userInfoManager.userEmailFlow.asLiveData().observe(viewLifecycleOwner,{
            email = it
            binding.etEmail.setText(it)
        })

        userInfoManager.userAddressFlow.asLiveData().observe(viewLifecycleOwner,{
            address = it
            binding.etAddress.setText(it)
        })

        userInfoManager.userPhoneFlow.asLiveData().observe(viewLifecycleOwner,{
            phone = it
            binding.etPhoneNumber.setText(it)
        })

    }
}
package com.example.assignmentfifteen

import android.content.Context
import androidx.datastore.DataStore
import androidx.datastore.preferences.Preferences
import androidx.datastore.preferences.createDataStore
import androidx.datastore.preferences.edit
import androidx.datastore.preferences.preferencesKey
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map

class UserInfoManager(context:Context) {

    private val dataStore: DataStore<Preferences> = context.createDataStore("user_prefs")




    suspend fun storeUser(firstName:String,lastName:String,age:Int,
                           gender:String,email:String,phone:String,address:String)
    {
        dataStore.edit {
            it[USER_FIRST_NAME_KEY] = firstName
            it[USER_LAST_NAME_KEY] = lastName
            it[USER_AGE_KEY] = age
            it[USER_GENDER_KEY] = gender
            it[USER_EMAIL_KEY] = email
            it[USER_PHONE_KEY] = phone
            it[USER_ADDRESS_KEY] = address
        }
    }

    val userFirstNameFlow : Flow<String> = dataStore.data.map {
        it[USER_FIRST_NAME_KEY] ?: ""
    }

    val userLastNameFlow : Flow<String> = dataStore.data.map {
        it[USER_LAST_NAME_KEY] ?: ""
    }

    val userAgeFlow : Flow<Int> = dataStore.data.map {
        it[USER_AGE_KEY] ?: 0
    }

    val userGenderFlow : Flow<String> = dataStore.data.map {
        it[USER_GENDER_KEY] ?: ""
    }

    val userEmailFlow : Flow<String> = dataStore.data.map {
        it[USER_EMAIL_KEY] ?: ""
    }

    val userPhoneFlow : Flow<String> = dataStore.data.map {
        it[USER_PHONE_KEY] ?: ""
    }

    val userAddressFlow : Flow<String> = dataStore.data.map {
        it[USER_ADDRESS_KEY] ?: ""
    }

    companion object
    {
        val USER_FIRST_NAME_KEY : Preferences.Key<String> = preferencesKey("USER_FIRST_NAME")
        val USER_LAST_NAME_KEY : Preferences.Key<String> = preferencesKey("USER_LAST_NAME")
        val USER_AGE_KEY : Preferences.Key<Int> = preferencesKey("USER_AGE")
        val USER_GENDER_KEY : Preferences.Key<String> = preferencesKey("USER_GENDER")
        val USER_EMAIL_KEY : Preferences.Key<String> = preferencesKey("USER_EMAIL")
        val USER_ADDRESS_KEY : Preferences.Key<String> = preferencesKey("USER_ADDRESS")
        val USER_PHONE_KEY : Preferences.Key<String> = preferencesKey("USER_PHONE")

    }


}